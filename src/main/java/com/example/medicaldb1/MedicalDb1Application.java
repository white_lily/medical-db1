package com.example.medicaldb1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicalDb1Application {

    public static void main(String[] args) {
        SpringApplication.run(MedicalDb1Application.class, args);
    }

}
