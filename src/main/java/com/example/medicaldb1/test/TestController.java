package com.example.medicaldb1.test;

import com.example.medicaldb1.pojo.request.ProteinBean;
import com.example.medicaldb1.pojo.system.RespBean;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class TestController {
    @PostMapping("/cdqglycmingxi_query")
    public RespBean cdqglycmingxi_query(@RequestBody ProteinBean proteinBean, HttpServletRequest request) {
        RespBean ret = new RespBean();
        ret.setCode(200);
        ret.setMessage("good");
        ret.setData("sdfsadf");
        return ret;
    }
}
