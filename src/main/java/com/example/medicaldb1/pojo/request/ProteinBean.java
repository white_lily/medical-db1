package com.example.medicaldb1.pojo.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProteinBean {
    String protein;
    String gwas;
    String clump;
    String p_val;
}
